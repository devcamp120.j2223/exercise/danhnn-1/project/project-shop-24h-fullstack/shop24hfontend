import Header from "../components/headerComponents/Header";
import Footer from "../components/footer/Footer";
import ProductDetail from "../components/content/ProductDetail";

function HomePage() {
  return (
    <>
      <Header />
      <ProductDetail />
      <Footer />
    </>
  );
}

export default HomePage;
