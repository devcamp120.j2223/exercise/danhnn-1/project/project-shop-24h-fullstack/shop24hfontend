import Header from "../components/headerComponents/Header";
import Footer from "../components/footer/Footer";
import ProductsContent from "../components/content/Products";
import ProductFilter from "../components/content/ProductFilter";
import { Container, Grid, Typography } from "@mui/material";

function ProductsPage() {
  return (
    <>
      <Header />
      <div
        style={{
          background: "#f5f5f5",
          padding: 0,
          margin: 0,
        }}
      >
        <Grid container>
          <Grid item xs="12" md="2">
            <ProductFilter />
          </Grid>
          <Grid item xs="12" md="10">
            <ProductsContent />
          </Grid>
        </Grid>
      </div>

      <Footer />
    </>
  );
}

export default ProductsPage;
