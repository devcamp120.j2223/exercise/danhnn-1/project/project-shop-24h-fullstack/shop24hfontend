import Header from "../components/headerComponents/Header";
import Footer from "../components/footer/Footer";
import CartContent from "../components/content/CartContent";
function CartPage() {
  return (
    <>
      <Header />
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          height: "95vh",
          justifyContent: "space-between",
        }}
      >
        <CartContent />
        <Footer />
      </div>
    </>
  );
}

export default CartPage;
