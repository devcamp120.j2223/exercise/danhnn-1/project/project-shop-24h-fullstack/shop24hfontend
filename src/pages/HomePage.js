import Header from "../components/headerComponents/Header";
import Footer from "../components/footer/Footer";

import HomeContent from "../components/content/Home";

function HomePage() {
  return (
    <>
      <Header />
      <HomeContent />
      <Footer />
    </>
  );
}

export default HomePage;
