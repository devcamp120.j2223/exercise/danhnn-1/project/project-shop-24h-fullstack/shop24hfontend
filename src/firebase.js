import firebase from "firebase";

// import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyAzs73jG9cBspGH1LdO346OrAOmwTcQy0s",
  authDomain: "shopaudio-67377.firebaseapp.com",
  projectId: "shopaudio-67377",
  storageBucket: "shopaudio-67377.appspot.com",
  messagingSenderId: "379542133752",
  appId: "1:379542133752:web:2b269cb91ef6eae18ed40d",
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider();
