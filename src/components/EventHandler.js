const initialState = {
  AllProducts: [],
  userData: null,
  name: "",
  type: "",
  mintPromotionPrice: 0,
  maxPromotionPrice: 0,
  ProductFilter: null,
  ListOrder: [],
};

const EventHandler = (state = initialState, action) => {
  switch (action.type) {
    case "SET_VALUE_USER_DATA": {
      return {
        ...state,
        userData: action.payload.userData,
      };
    }
    case "SET_VALUE_ALL_PRODUCTS": {
      return {
        ...state,
        AllProducts: [...action.payload.AllProducts],
      };
    }
    case "SET_VALUE_MIN_PRICE": {
      return {
        ...state,
        mintPromotionPrice: action.payload.mintPromotionPrice,
      };
    }
    case "SET_VALUE_MAX_PRICE": {
      return {
        ...state,
        maxPromotionPrice: action.payload.maxPromotionPrice,
      };
    }
    case "SET_VALUE_NAME": {
      return {
        ...state,
        name: action.payload.name,
      };
    }
    case "SET_VALUE_PRODUCTS_FILTER": {
      return {
        ...state,
        ProductFilter: action.payload.ProductFilter,
      };
    }
    case "SET_PRODUCT_TYPE": {
      return {
        ...state,
        type: action.payload.type,
      };
    }
    case "SET_VALUE_LIST_ORDER": {
      return {
        ...state,
        ListOrder: [...state.ListOrder, action.payload.ListOrder],
      };
    }
    case "SET_VALUE_LIST_ORDER_1": {
      return {
        ...state,
        ListOrder: [...action.payload.ListOrder],
      };
    }
    default: {
      return state;
    }
  }
};
export default EventHandler;
