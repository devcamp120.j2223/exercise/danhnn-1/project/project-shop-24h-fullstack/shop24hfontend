import {
  Grid,
  Typography,
  FormGroup,
  TextField,
  FormControlLabel,
  RadioGroup,
  Radio,
} from "@mui/material";
import { Col, Row } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
function ProductFilter() {
  const dispatch = useDispatch();
  const { name, type, mintPromotionPrice, maxPromotionPrice } = useSelector(
    (reduxData) => reduxData.taskReducer
  );

  const setMintPromotionPrice = (e) => {
    if (e.target.value < 0) {
      dispatch({
        type: "SET_VALUE_MIN_PRICE",
        payload: {
          mintPromotionPrice: 0,
        },
      });
    } else {
      dispatch({
        type: "SET_VALUE_MIN_PRICE",
        payload: {
          mintPromotionPrice: e.target.value,
        },
      });
    }
  };

  const setMaxPromotionPrice = (e) => {
    if (e.target.value < 0) {
      dispatch({
        type: "SET_VALUE_MIN_PRICE",
        payload: {
          mintPromotionPrice: 0,
        },
      });
    } else {
      dispatch({
        type: "SET_VALUE_MAX_PRICE",
        payload: {
          maxPromotionPrice: e.target.value,
        },
      });
    }
  };

  const handleChange = (e) => {
    if (e.target.value === name) {
      dispatch({
        type: "SET_VALUE_NAME",
        payload: {
          name: "",
        },
      });
    } else {
      dispatch({
        type: "SET_VALUE_NAME",
        payload: {
          name: e.target.value,
        },
      });
    }
  };

  const handleChangeType = (e) => {
    dispatch({
      type: "SET_PRODUCT_TYPE",
      payload: {
        type: e.target.value,
      },
    });
  };

  const callApiGetDataFilter = async () => {
    const response = await fetch(
      `http://localhost:8000/products?name=${name}&type=${type}&minPromotionPrice=${mintPromotionPrice}&maxPromotionPrice=${maxPromotionPrice}`
    );
    await response
      .json()
      .then((data) => {
        dispatch({
          type: "SET_VALUE_PRODUCTS_FILTER",
          payload: {
            ProductFilter: data.data,
          },
        });
      })
      .catch((error) => {
        console.error(error.message);
      });
    // }
  };

  useEffect(() => {
    callApiGetDataFilter();
  }, [mintPromotionPrice, maxPromotionPrice, name, type]);

  return (
    <div style={{ height: "100%", padding: "10vh 1vw" }}>
      <Grid container>
        <Typography
          sx={{ fontSize: { xs: "6vw", md: "1.5vw" }, fontWeight: "bold" }}
          style={{ margin: "0 auto" }}
        >
          LỌC SẢN PHẨM
        </Typography>
      </Grid>
      <FormGroup style={{ marginBottom: "20px" }}>
        <Grid container>
          <Typography
            sx={{ fontSize: { xs: "6vw", md: "1.5vw" } }}
            variant="h6"
          >
            Giá:
          </Typography>
        </Grid>
        <Grid mt={1} container>
          <Row>
            <Col>
              <TextField
                size="small"
                type="Number"
                label="Giá Min"
                variant="outlined"
                onChange={setMintPromotionPrice}
                value={mintPromotionPrice}
              />
            </Col>
            -
            <Col>
              <TextField
                size="small"
                type="Number"
                label="Giá Max"
                variant="outlined"
                onChange={setMaxPromotionPrice}
                value={maxPromotionPrice}
              />
            </Col>
          </Row>
        </Grid>
      </FormGroup>

      <FormGroup>
        <Grid container>
          <Typography
            sx={{ fontSize: { xs: "6vw", md: "1.5vw" } }}
            variant="h6"
          >
            Brands:
          </Typography>
        </Grid>
        <Grid container>
          <FormGroup>
            <RadioGroup
              sx={{
                flexDirection: { xs: "row", md: "column" },
              }}
              defaultValue=""
            >
              <FormControlLabel
                value=""
                control={<Radio onChange={handleChange} />}
                label="ALL"
              />
              <FormControlLabel
                value="Custom"
                control={<Radio onChange={handleChange} />}
                label="Custom"
              />
              <FormControlLabel
                value="Mini"
                control={<Radio onChange={handleChange} />}
                label="Mini"
              />
            </RadioGroup>
          </FormGroup>
        </Grid>
      </FormGroup>

      <FormGroup>
        <Grid container>
          <Typography
            sx={{ fontSize: { xs: "6vw", md: "1.5vw" } }}
            variant="h6"
          >
            Loại:
          </Typography>
        </Grid>
        <Grid container>
          <FormGroup>
            <RadioGroup
              sx={{
                flexDirection: { xs: "row", md: "column" },
              }}
              defaultValue=""
            >
              <FormControlLabel
                value=""
                control={<Radio onChange={handleChangeType} />}
                label="ALL"
              />
              <FormControlLabel
                value="Classic"
                control={<Radio onChange={handleChangeType} />}
                label="Classic"
              />
              <FormControlLabel
                value="Acoustic"
                control={<Radio onChange={handleChangeType} />}
                label="Acoustic"
              />
            </RadioGroup>
          </FormGroup>
        </Grid>
      </FormGroup>
    </div>
  );
}

export default ProductFilter;
