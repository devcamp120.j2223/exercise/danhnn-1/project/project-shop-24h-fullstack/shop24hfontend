import { Grid, Typography, Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import GoogleIcon from "@mui/icons-material/Google";
import { useDispatch, useSelector } from "react-redux";
import { auth, googleProvider } from "../../firebase";
import { useNavigate } from "react-router-dom";
function LogInContent() {
  const dispatch = useDispatch();
  let navigate = useNavigate();

  const loginGoogle = async () => {
    await auth
      .signInWithPopup(googleProvider)
      .then((result) => {
        dispatch({
          type: "SET_VALUE_USER_DATA",
          payload: {
            userData: result.user,
          },
        });
      })
      .catch((error) => {
        console.log(error);
      });

    navigate("/cart");
  };
  return (
    <div style={{ display: "flex", height: "100vh", justifyContent: "center" }}>
      <Grid className="d-flex justify-content-center align-items-center">
        <Grid
          item
          className=" mx-auto text-center p-5"
          style={{ backgroundColor: "#dee6e9" }}
        >
          <div
            className="p-5"
            style={{ backgroundColor: "white", borderRadius: "8px" }}
          >
            <Grid
              container
              className="d-flex align-items-center justify-content-center"
            >
              <Grid item mb={5}>
                <Button
                  onClick={() => {
                    loginGoogle();
                  }}
                  variant="contained"
                  style={{
                    backgroundColor: "red",
                    color: "white",
                    borderRadius: "32px",
                  }}
                >
                  <GoogleIcon />{" "}
                  <Typography
                    sx={{
                      fontSize: { xs: "3vw", md: "1vw" },
                      margin: "0 5px",
                    }}
                  >
                    Sign in with{" "}
                  </Typography>
                  <Typography sx={{ fontSize: { xs: "4vw", md: "2vw" } }}>
                    Google
                  </Typography>
                </Button>
              </Grid>
            </Grid>

            <hr></hr>

            <div
              className="d-flex align-items-center justify-content-center mx-auto"
              style={{
                width: "40px",
                height: "40px",
                borderRadius: "50%",
                border: "1px solid black",
                marginTop: "-35px",
                backgroundColor: "#000000",
                color: "white",
              }}
            >
              <Typography variant="h6">or</Typography>
            </div>

            <Grid container mt={5}>
              <Grid item xs={12}>
                <TextField
                  size="small"
                  fullWidth
                  label="Username"
                  variant="outlined"
                />
              </Grid>
            </Grid>

            <Grid container mt={3}>
              <Grid item xs={12}>
                <TextField
                  size="small"
                  fullWidth
                  label="Password"
                  variant="outlined"
                />
              </Grid>
            </Grid>

            <Grid
              container
              className="d-flex align-items-center justify-content-center mt-4"
            >
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  className="w-100 p-2"
                  color="success"
                  style={{ borderRadius: "32px" }}
                >
                  <b style={{ fontSize: "18px" }}>Sign in</b>
                </Button>
              </Grid>
            </Grid>
          </div>
          <Grid container mt={3}>
            <Grid item xs={12}>
              <Typography>
                Don't have an account?{" "}
                <a
                  href="/"
                  style={{ color: "#67bc75", textDecoration: "none" }}
                >
                  Sign up here
                </a>
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}

export default LogInContent;
