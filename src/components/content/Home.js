import {
  Container,
  Grid,
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
  Button,
  Link,
} from "@mui/material";
import {
  Row,
  Col,
  Carousel,
  CarouselIndicators,
  CarouselItem,
  CarouselControl,
} from "reactstrap";

import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import slide1 from "../../assets/images/1.jpg";
import slide2 from "../../assets/images/2.jpg";
import slide3 from "../../assets/images/3.jpg";
import slide4 from "../../assets/images/4.png";
import slide5 from "../../assets/images/5.png";
import slide6 from "../../assets/images/6.png";
import LocalFireDepartmentIcon from "@mui/icons-material/LocalFireDepartment";
function Home() {
  // ---------slider------------------------------
  const [activeIndex, setActiveIndex] = useState(0);
  // Previous button for Carousel
  const previousButton = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? itemLength : activeIndex - 1;
    setActiveIndex(nextIndex);
  };
  // Next button for Carousel
  const nextButton = () => {
    if (animating) return;
    const nextIndex = activeIndex === itemLength ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };
  // Carousel Item Data
  const items = [
    {
      caption: "Sample Caption One",
      src: slide1,
      altText: "Slide One",
    },
    {
      caption: "Sample Caption Two",
      src: slide2,
      altText: "Slide Two",
    },
    {
      caption: "Sample Caption Two",
      src: slide3,
      altText: "Slide Three",
    },
    {
      caption: "Sample Caption Two",
      src: slide4,
      altText: "Slide Four",
    },
  ];
  // Items array length
  const itemLength = items.length - 1;
  const carouselItemData = items.map((item, index) => {
    return (
      <CarouselItem
        key={index}
        onExited={() => setAnimating(false)}
        onExiting={() => setAnimating(true)}
        className="img-slide"
      >
        <img src={item.src} alt={item.altText} />
      </CarouselItem>
    );
  });
  // State for Animation
  const [animating, setAnimating] = useState(false);
  // ---------slider-------------------------------

  //---------Products------------------------------
  // store
  const dispatch = useDispatch();
  const { AllProducts, userData } = useSelector(
    (reduxData) => reduxData.taskReducer
  );

  const fetchAPI = async (url) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  };

  useEffect(() => {
    fetchAPI("http://localhost:8000/products/?limit=10")
      .then((data) => {
        dispatch({
          type: "SET_VALUE_ALL_PRODUCTS",
          payload: {
            AllProducts: data.data,
          },
        });
      })
      .catch((error) => {
        console.error(error.message);
      });
  }, [userData]);

  // const btnShowAll = () => {
  //   fetchAPI("http://localhost:8000/products")
  //     .then((data) => {
  //       dispatch({
  //         type: "SET_VALUE_ALL_PRODUCTS",
  //         payload: {
  //           AllProducts: data.data,
  //         },
  //       });
  //     })
  //     .catch((error) => {
  //       console.error(error.message);
  //     });
  // };
  //---------Products------------------------------

  return (
    <div
      style={{
        background: "#f5f5f5",
      }}
    >
      {/* carousel */}
      <Container style={{ paddingTop: "60px" }}>
        <Row>
          <Col xs={12} lg={7} md={7} sm={12}>
            <Carousel
              previous={previousButton}
              next={nextButton}
              activeIndex={activeIndex}
            >
              <CarouselIndicators
                items={items}
                activeIndex={activeIndex}
                onClickHandler={(newIndex) => {
                  if (animating) return;
                  setActiveIndex(newIndex);
                }}
              />
              {carouselItemData}
              <CarouselControl
                directionText="Prev"
                direction="prev"
                onClickHandler={previousButton}
              />
              <CarouselControl
                directionText="Next"
                direction="next"
                onClickHandler={nextButton}
              />
            </Carousel>
          </Col>
          <Col xs={12} lg={5} md={5} sm={12}>
            <Col xs={12}>
              <Link href="/products">
                <img
                  src={slide5}
                  width="100%"
                  height="200"
                  className="hover-slide"
                  style={{ borderRadius: "20px" }}
                />
              </Link>
            </Col>
            <Col xs={12} className="mt-3">
              <Link href="/products">
                <img
                  src={slide6}
                  width="100%"
                  height="200"
                  className="hover-slide"
                  style={{ borderRadius: "20px" }}
                />
              </Link>
            </Col>
          </Col>
        </Row>
      </Container>
      <Container>
        <Grid item xs={12} mt={6} p={2} sx={{ backgroundColor: "red" }}>
          <Typography variant="h6" align="center" color="white">
            <b>Hàng Chính Hãng - Made In Việt Nam</b>
          </Typography>
        </Grid>
      </Container>
      {/* product */}
      <Container style={{ paddingBottom: "2vh" }}>
        <Grid container>
          <Grid item xs={12} mt={12} p={2}>
            <Typography variant="h6">
              <b>
                Các Mẫu Bán Chạy{" "}
                <LocalFireDepartmentIcon style={{ color: "red" }} />{" "}
              </b>
            </Typography>
          </Grid>
          {AllProducts.map((product, index) => {
            return (
              <Grid
                key={index}
                item
                sx={{
                  width: { xs: "100%", md: "20%" },
                }}
                className="p-3"
              >
                <Link
                  href={`/products/${product._id}`}
                  style={{ textDecoration: "none" }}
                >
                  <div className="home-card" style={{ borderRadius: "15px " }}>
                    {/* <Card> */}
                    <CardActionArea>
                      <CardMedia
                        style={{ borderRadius: "15px 15px 0 0" }}
                        component="img"
                        image={product.imageUrl}
                        alt="Img Guitar"
                      />
                      <CardContent>
                        <Typography
                          gutterBottom
                          variant="h6"
                          sx={{ fontSize: { xs: "5vw", md: "1.3vw" } }}
                          style={{
                            height: "33px",
                            color: "black",
                          }}
                          component="div"
                          align="center"
                        >
                          <b> {product.name}</b>
                        </Typography>
                        <Typography
                          variant="body2"
                          color="text.secondary"
                          mt={2}
                          align="center"
                          sx={{ fontSize: { xs: "4vw", md: "1.1vw" } }}
                        >
                          <strike>{product.buyPrice.toLocaleString()}đ</strike>
                        </Typography>
                        <Typography
                          align="center"
                          variant="h6"
                          sx={{
                            color: "red",
                            fontSize: { xs: "4.5vw", md: "1.2vw" },
                          }}
                        >
                          <b>{product.promotionPrice.toLocaleString()}đ</b>
                        </Typography>
                      </CardContent>
                    </CardActionArea>
                    {/* </Card> */}
                  </div>
                </Link>
              </Grid>
            );
          })}
        </Grid>

        <Grid item xs={12} style={{ textAlign: "center" }} mt={5}>
          <Link style={{ textDecoration: "none" }} href={"/products"}>
            <Button variant="contained" style={{ backgroundColor: "#fcb900" }}>
              Tất Cả Sản Phẩm
            </Button>
          </Link>
        </Grid>
      </Container>
    </div>
  );
}

export default Home;
