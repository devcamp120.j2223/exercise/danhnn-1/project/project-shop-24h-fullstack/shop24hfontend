import {
  Container,
  Grid,
  Typography,
  CardActionArea,
  CardMedia,
  CardContent,
  Pagination,
} from "@mui/material";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

function Products() {
  const dispatch = useDispatch();
  const { AllProducts, ProductFilter } = useSelector(
    (reduxData) => reduxData.taskReducer
  );
  //Pagination
  const limit = 8;
  const [page, setPage] = useState(1);
  const [noPage, setNoPage] = useState(0);
  const [show, setShow] = useState([]);
  const changePageHandler = (event, value) => {
    setPage(value);
  };

  useEffect(() => {
    if (ProductFilter === null) {
      setNoPage(Math.ceil(AllProducts.length / limit));
      setShow(AllProducts.slice((page - 1) * limit, page * limit));
    } else {
      setNoPage(Math.ceil(ProductFilter.length / limit));
      setShow(ProductFilter.slice((page - 1) * limit, page * limit));
    }
  }, [page, ProductFilter]);

  return (
    <Container style={{ margin: "10vh auto" }} className="mb-5">
      <Grid container>
        {/* {(ProductFilter == null ? AllProducts : ProductFilter).map( */}
        {show.map((product, index) => {
          return (
            <Grid key={index} item xs={12} md={3} className="p-2">
              <Link
                // onClick={() => {
                //   clickProductDetail();
                // }}
                to={product._id}
                style={{ textDecoration: "none" }}
              >
                {/* <Card> */}
                <div
                  className="home-card"
                  style={{ borderRadius: "15px ", backgroundColor: "white" }}
                >
                  <CardActionArea>
                    <CardMedia
                      style={{ borderRadius: "15px 15px 0 0" }}
                      component="img"
                      image={product.imageUrl}
                      alt="green iguana"
                    />
                    <CardContent>
                      <Typography
                        gutterBottom
                        variant="h6"
                        style={{ height: "30px", color: "black" }}
                        component="div"
                        align="center"
                        sx={{ fontSize: { xs: "4vw", md: "1.1vw" } }}
                      >
                        <b> {product.name}</b>
                      </Typography>
                      <Typography
                        variant="body2"
                        color="text.secondary"
                        mt={2}
                        align="center"
                        sx={{ fontSize: { xs: "4vw", md: "1.1vw" } }}
                      >
                        <strike>{product.buyPrice.toLocaleString()}đ</strike>
                      </Typography>
                      <Typography
                        align="center"
                        variant="h6"
                        sx={{
                          color: "red",
                          fontSize: { xs: "4.5vw", md: "1.2vw" },
                        }}
                      >
                        <b>{product.promotionPrice.toLocaleString()}đ</b>
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  {/* </Card> */}
                </div>
                {/* </Card> */}
              </Link>
            </Grid>
          );
        })}
      </Grid>

      <Grid container mt={2} mb={5} justifyContent="end">
        <Grid item>
          <Pagination
            color="secondary"
            count={noPage}
            defaultPage={page}
            onChange={changePageHandler}
          ></Pagination>
        </Grid>
      </Grid>
    </Container>
  );
}
export default Products;
