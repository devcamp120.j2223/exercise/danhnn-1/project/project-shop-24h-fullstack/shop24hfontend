import {
  Container,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Button,
  Typography,
  Modal,
  Box,
  TextField,
  ButtonGroup,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
function CartContent() {
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    minWidth: "280px",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    border: "2px solid #f5f5f5",
    borderRadius: "20px",
    boxShadow: 24,
    p: 4,
  };
  let navigate = useNavigate();
  const dispatch = useDispatch();
  const { ListOrder, userData } = useSelector(
    (reduxData) => reduxData.taskReducer
  );
  const [orderList, setOrderList] = useState("");
  const [price, setPrice] = useState(0);
  const [open, setOpen] = useState(false);
  const [open1, setOpen1] = useState(false);
  const [open2, setOpen2] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const handleClose1 = () => setOpen1(false);
  const handleClose2 = () => setOpen2(false);
  const [userDb, setUserDb] = useState({});
  const [alerStr, setAlertStr] = useState({ title: "", str: "" });
  const [newOrder, setNewOrder] = useState({
    _id: "",
    orderDate: "",
    shippedDate: "",
    orderDetail: [],
    cost: 0,
  });

  const fetchAPI = async (url) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  };
  const onChangeQualityUp = (data) => {
    if (data.quality >= 0) {
      setOrderList(data.quality++);
      console.log(data);
    } else {
      setOrderList(0);
    }

    localStorage.setItem("ListOrder", JSON.stringify(ListOrder));
  };
  const onChangeQualityDown = (data) => {
    if (data.quality > 0) {
      setOrderList(data.quality--);
    } else if (data.quality === 0) {
      setOrderList(0);
    }
    localStorage.setItem("ListOrder", JSON.stringify(ListOrder));
  };
  const btnBuyClick = () => {
    if (userData === null) {
      navigate("/login");
    }
    setUserDb({ fullName: userData.displayName, email: userData.email });
    handleOpen();
    // localStorage.setItem("ListOrder", JSON.stringify([]));
  };
  const onChangeName = (event) => {
    setUserDb({
      fullName: event.target.value,
      email: userDb.email,
      phone: userDb.phone,
      address: userDb.address,
      shippedDate: userDb.shippedDate,
      note: userDb.note,
    });
  };
  const onChangeEmail = (event) => {
    setUserDb({
      fullName: userDb.fullName,
      email: event.target.value,
      phone: userDb.phone,
      address: userDb.address,
      shippedDate: userDb.shippedDate,
      note: userDb.note,
    });
  };
  const onChangePhone = (event) => {
    setUserDb({
      fullName: userDb.fullName,
      email: userDb.email,
      phone: event.target.value,
      address: userDb.address,
      shippedDate: userDb.shippedDate,
      note: userDb.note,
    });
  };
  const onChangeAddress = (event) => {
    setUserDb({
      fullName: userDb.fullName,
      email: userDb.email,
      phone: userDb.phone,
      address: event.target.value,
      shippedDate: userDb.shippedDate,
      note: userDb.note,
    });
  };
  const onChangeShippedDate = (event) => {
    setUserDb({
      fullName: userDb.fullName,
      email: userDb.email,
      phone: userDb.phone,
      address: userDb.address,
      shippedDate: event.target.value,
      note: userDb.note,
    });
  };
  const onChangeNote = (event) => {
    setUserDb({
      fullName: userDb.fullName,
      email: userDb.email,
      phone: userDb.phone,
      address: userDb.address,
      shippedDate: userDb.shippedDate,
      note: event.target.value,
    });
  };
  const validateDataUserInput = () => {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (userDb.fullName.trim().length <= 1) {
      setAlertStr({
        title: "Cảnh báo sai tên",
        str: "Bạn Hãy Nhập Lại Họ và Tên!",
      });
      setOpen1(true);
      return false;
    }
    if (!userDb.email.match(mailformat)) {
      setAlertStr({
        title: "Cảnh báo sai email",
        str: "Bạn Hãy Nhập Lại Email!",
      });
      setOpen1(true);
      return false;
    }
    if (
      !userDb.phone.match(
        /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im
      )
    ) {
      setAlertStr({
        title: "Cảnh báo sai số điện thoại",
        str: "Bạn Hãy Nhập Lại Số Điện Thoại!",
      });
      setOpen1(true);
      return false;
    }
    if (userDb.address === undefined || userDb.address.trim().length === 0) {
      setAlertStr({
        title: "Cảnh báo sai địa chỉ",
        str: "Bạn Hãy Nhập Lại địa chỉ!",
      });
      setOpen1(true);
      return false;
    }
    return true;
  };
  const btnConfirmClick = async () => {
    let validate = validateDataUserInput();
    if (validate) {
      callApiCheckUser(userDb.phone);
    }
  };
  const callApiCreateUser = async () => {
    const response = await fetch(`http://localhost:8000/customers`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        fullName: userDb.fullName,
        phone: userDb.phone,
        email: userDb.email,
        address: userDb.address,
      }),
    });
    const data = await response.json();
    console.log(data);
    return data;
  };

  const callApiCheckUser = async (sdt) => {
    const getUserDb = await fetchAPI(`http://localhost:8000/customers/${sdt}`);
    if (getUserDb.data.length === 0) {
      const newUser = await callApiCreateUser();
      await callApiCreateOrder(newUser.data);
    } else {
      await callApiCreateOrder(getUserDb.data[0]);
    }
  };
  const callApiCreateOrder = async (data) => {
    const Response = await fetch(
      `http://localhost:8000/customers/${data._id}/orders`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          shippedDate: userDb.shippedDate,
          note: userDb.note,
          orderDetail: ListOrder,
          cost: price,
        }),
      }
    );
    const content = await Response.json();
    if (content.data) {
      await setNewOrder(content.data);
      handleClose();
      setOpen2(true);
      localStorage.setItem("ListOrder", JSON.stringify([]));
    }
  };

  useEffect(() => {
    let items = JSON.parse(localStorage.getItem("ListOrder"));
    if (ListOrder.length === 0) {
      if (items.length > 0) {
        dispatch({
          type: "SET_VALUE_LIST_ORDER_1",
          payload: {
            ListOrder: items,
          },
        });
      }
    } else {
      localStorage.setItem("ListOrder", JSON.stringify(ListOrder));
    }
    let p = 0;
    ListOrder.forEach((element) => {
      p += element.promotionPrice * element.quality;
    });
    setPrice(p);
  }, [ListOrder, orderList]);

  return (
    <Container style={{ marginTop: "30px" }}>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650, fontSize: "2rem" }} size="small">
          <TableHead>
            <TableRow>
              <TableCell align="center" width={"20%"}>
                Hình Ảnh
              </TableCell>
              <TableCell align="center">Tên Sản Phẩm</TableCell>
              <TableCell align="center">Giá Sản Phẩm</TableCell>
              <TableCell align="center">Thành Tiền</TableCell>
              <TableCell align="center">Số Lượng</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {ListOrder.map((order, index) => (
              <TableRow
                key={order.name}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell>
                  <img
                    style={{ width: "100%" }}
                    src={order.imageUrl}
                    alt={"img"}
                  />
                </TableCell>
                <TableCell align="center">{order.name}</TableCell>
                <TableCell align="center">{order.promotionPrice}</TableCell>
                <TableCell align="center">
                  {(order.quality * order.promotionPrice).toLocaleString()}
                </TableCell>
                <TableCell align="center">
                  <Button
                    onClick={() => {
                      onChangeQualityUp(order);
                    }}
                    style={{ fontWeight: "bold" }}
                  >
                    +
                  </Button>
                  <p key={index}>{order.quality}</p>
                  <Button
                    onClick={() => {
                      onChangeQualityDown(order);
                    }}
                    style={{ fontWeight: "bold" }}
                  >
                    -
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Grid item m={4} display={"flex"}>
        <Typography
          mr={2}
          sx={{ fontSize: { xs: "4vw", md: "1.5vw" } }}
          fontWeight={"bold"}
        >
          Tổng tiền: {price.toLocaleString()} VNĐ
        </Typography>
        <Button
          sx={{ fontSize: { xs: "3.3vw", md: "1.5vw", padding: "3px" } }}
          onClick={() => {
            btnBuyClick();
          }}
          variant="contained"
        >
          Mua Hàng
        </Button>
      </Grid>

      <Modal open={open} onClose={handleClose}>
        <Box sx={style}>
          <Typography variant="h6" component="h2">
            Thông tin Khách Hàng
          </Typography>
          <TextField
            style={{ margin: "10px auto", width: "100%" }}
            label="Họ Tên"
            variant="outlined"
            value={userDb.fullName}
            onChange={onChangeName}
          />
          <TextField
            style={{ margin: "10px auto", width: "100%" }}
            label="Email"
            variant="outlined"
            value={userDb.email}
            onChange={onChangeEmail}
          />
          <TextField
            style={{ margin: "10px auto", width: "100%" }}
            label="SĐT"
            variant="outlined"
            value={userDb.phone}
            onChange={onChangePhone}
          />
          <TextField
            style={{ margin: "10px auto", width: "100%" }}
            label="Địa Chỉ"
            variant="outlined"
            value={userDb.address}
            onChange={onChangeAddress}
          />
          <TextField
            type="date"
            style={{ margin: "10px auto", width: "100%" }}
            label="ngày vận chuyển"
            variant="outlined"
            InputLabelProps={{
              shrink: true,
            }}
            value={userDb.shippedDate}
            onChange={onChangeShippedDate}
          />
          <TextField
            style={{ margin: "10px auto", width: "100%" }}
            label="Ghi chú"
            variant="outlined"
            value={userDb.note}
            onChange={onChangeNote}
          />
          <ButtonGroup
            variant="contained"
            aria-label="outlined primary button group"
          >
            <Button
              onClick={() => {
                btnConfirmClick();
              }}
            >
              Xác Nhận
            </Button>
            <Button
              onClick={() => {
                handleClose();
              }}
              color="error"
            >
              Hủy
            </Button>
          </ButtonGroup>
        </Box>
      </Modal>
      <Modal
        open={open1}
        onClose={handleClose1}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            {alerStr.title}
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            {alerStr.str}
          </Typography>
        </Box>
      </Modal>
      <Modal
        open={open2}
        onClose={handleClose2}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Thông tin đơn hàng
          </Typography>
          <Typography
            id="modal-modal-description"
            sx={{ mt: 2, fontSize: { xs: "4.5vw", md: "1.2vw" } }}
          >
            Mã đơn hàng: {newOrder._id}
          </Typography>
          <Typography
            id="modal-modal-description"
            sx={{ mt: 2, fontSize: { xs: "4.5vw", md: "1.2vw" } }}
          >
            Ngày tạo đơn: {newOrder.orderDate}
          </Typography>
          <Typography
            id="modal-modal-description"
            sx={{ mt: 2, fontSize: { xs: "4.5vw", md: "1.2vw" } }}
          >
            Ngày giao: {newOrder.shippedDate}
          </Typography>
          <Typography
            id="modal-modal-description"
            sx={{ mt: 2, fontSize: { xs: "4.5vw", md: "1.2vw" } }}
          >
            Tổng tiền: {newOrder.cost.toLocaleString()} VNĐ
          </Typography>
          <Typography
            id="modal-modal-description"
            sx={{ mt: 2, fontSize: { xs: "4.5vw", md: "1.2vw" } }}
          >
            Sản phẩm:{" "}
            {newOrder.orderDetail.map((element) => {
              return " " + element.name + " " + "số lượng: " + element.quality;
            })}
          </Typography>
        </Box>
      </Modal>
    </Container>
  );
}

export default CartContent;
