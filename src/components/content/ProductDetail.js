import {
  Container,
  Grid,
  Typography,
  ButtonGroup,
  Button,
  Rating,
  Box,
  Modal,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

function ProductDetail() {
  //Link đến trang theo ID
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    minWidth: "280px",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    border: "2px solid #f5f5f5",
    borderRadius: "20px",
    boxShadow: 24,
    p: 4,
  };
  const [value, setValue] = useState(4);
  const [open1, setOpen1] = useState(false);
  const handleClose1 = () => setOpen1(false);
  const [bill, setBill] = useState(0);
  const { productId } = useParams();
  const dispatch = useDispatch();
  const { ListOrder } = useSelector((reduxData) => reduxData.taskReducer);
  const [card, setCard] = useState({});
  const [quality, setQuality] = useState(0);
  const fetchAPI = async (url) => {
    const response = await fetch(url);
    const data = await response.json();
    return data;
  };
  const BtnAddCartClick = async () => {
    if (ListOrder.length === 0) {
      await dispatch({
        type: "SET_VALUE_LIST_ORDER",
        payload: {
          ListOrder: { ...card, quality: quality },
        },
      });
      setBill(0);
      setQuality(0);
      await localStorage.setItem("ListOrder", JSON.stringify(ListOrder));
    } else {
      let check = 0;
      ListOrder.forEach((element) => {
        if (element._id === productId) {
          check++;
          element.quality += quality;
        }
      });
      if (check === 0) {
        await dispatch({
          type: "SET_VALUE_LIST_ORDER",
          payload: {
            ListOrder: { ...card, quality: quality },
          },
        });
        setBill(0);
        setQuality(0);
        await localStorage.setItem("ListOrder", JSON.stringify(ListOrder));
      } else {
        dispatch({
          type: "SET_VALUE_LIST_ORDER_1",
          payload: {
            ListOrder: ListOrder,
          },
        });
      }
    }
  };

  useEffect(() => {
    setBill(quality === 0 ? 0 : quality * card.promotionPrice);
  }, [quality]);

  useEffect(() => {
    fetchAPI("http://localhost:8000/products/" + productId)
      .then((data) => {
        setCard(data.data);
      })
      .catch((error) => {
        console.error(error.message);
      });
    let items = JSON.parse(localStorage.getItem("ListOrder"));
    if (items.length > 0 && ListOrder.length === 0) {
      dispatch({
        type: "SET_VALUE_LIST_ORDER_1",
        payload: {
          ListOrder: items,
        },
      });
    }
  });

  return (
    <Container>
      <Grid container mt={5}>
        <Grid item xs={12} md={3} mb={1}>
          <img
            src={card.imageUrl}
            style={{ width: "100%", borderRadius: "14px" }}
            alt="img"
          />
        </Grid>

        <Grid item xs={1}></Grid>

        <Grid item xs={12} md={8} className="p-1">
          <Grid container>
            <Grid item xs={12}>
              <Typography sx={{ fontSize: { xs: "5vw", md: "1.5vw" } }}>
                <b>{card.name}</b>
              </Typography>
            </Grid>
          </Grid>

          <Grid mt={1} container>
            <Grid item xs={12}>
              <Box
                sx={{
                  display: "flex",
                }}
              >
                <Typography variant="p" style={{ fontSize: "18px" }}>
                  Đánh giá{" "}
                </Typography>
                <Rating
                  value={value}
                  onChange={(event, newValue) => {
                    setValue(newValue);
                  }}
                />
              </Box>
            </Grid>
          </Grid>

          <Grid container mt={1}>
            <Grid item xs={12}>
              <Typography sx={{ fontSize: { xs: "5vw", md: "1.5vw" } }}>
                Giá niêm yết:{" "}
                <strike>
                  <b style={{ opacity: 0.7 }}>
                    {Number(card.buyPrice).toLocaleString()} VNĐ
                  </b>
                </strike>
              </Typography>
            </Grid>
          </Grid>

          <Grid container>
            <Grid item xs={12}>
              <Typography sx={{ fontSize: { xs: "5.6vw", md: "2vw" } }}>
                <b style={{ color: "red" }}>
                  Giá Khuyến mãi: {Number(card.promotionPrice).toLocaleString()}{" "}
                  VNĐ
                </b>
              </Typography>
            </Grid>
          </Grid>

          <Grid container mt={1}>
            <Grid item xs={12}>
              <Typography mb={1} sx={{ fontSize: { xs: "5vw", md: "1.5vw" } }}>
                Số lượng
              </Typography>
              <ButtonGroup
                variant="contained"
                aria-label="outlined primary button group"
              >
                <Button
                  onClick={() => {
                    quality === 0 ? setQuality(0) : setQuality(quality - 1);
                  }}
                >
                  -
                </Button>
                <Typography
                  align="center"
                  marginTop={"8px"}
                  style={{ width: "50px" }}
                >
                  {quality}
                </Typography>
                <Button
                  onClick={() => {
                    setQuality(quality + 1);
                  }}
                >
                  +
                </Button>
              </ButtonGroup>
            </Grid>
          </Grid>

          <Grid container mt={3}>
            <Grid item xs={12}>
              <ButtonGroup
                variant="contained"
                aria-label="outlined primary button group"
              >
                <Button
                  onClick={() => {
                    BtnAddCartClick();
                    setOpen1(true);
                  }}
                >
                  Thêm Vào Giỏ Hàng
                </Button>
              </ButtonGroup>
            </Grid>
          </Grid>

          <Grid container mt={3}>
            <Grid item xs={12}>
              <Typography
                border={"1px #1976d2 solid"}
                minWidth={"50%"}
                padding={1}
                borderRadius={3}
                sx={{ fontSize: { xs: "5vw", md: "1.5vw" } }}
                style={{ maxWidth: "250px" }}
              >
                {"Thành tiền: " + bill.toLocaleString()}
              </Typography>
            </Grid>
          </Grid>
          <Grid xs={12} className="pt-2" style={{ borderRadius: "14px" }}>
            <Grid xs={12}>
              <Typography sx={{ fontSize: { xs: "5vw", md: "1.5vw" } }}>
                <b>Description</b>
              </Typography>
            </Grid>
            <Grid item xs={12} md={10} mt={1} pb={3}>
              <Typography
                sx={{ fontSize: { xs: "4vw", md: "1.2vw" } }}
                variant="body1"
              >
                {card.description}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <Modal
        open={open1}
        onClose={handleClose1}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Bạn đã thêm vào Giỏ hàng thành công
          </Typography>
        </Box>
      </Modal>
    </Container>
  );
}

export default ProductDetail;
