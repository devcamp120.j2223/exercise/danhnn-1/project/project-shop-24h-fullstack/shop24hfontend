import { Container, Grid, Typography, Link } from "@mui/material";

import FacebookIcon from "@mui/icons-material/Facebook";
import InstagramIcon from "@mui/icons-material/Instagram";
import YouTubeIcon from "@mui/icons-material/YouTube";
import TwitterIcon from "@mui/icons-material/Twitter";

function Footer() {
  const title = [
    "Help Center",
    "Contact Us",
    "Product Help",
    "Warranty",
    "Order Status",
  ];

  return (
    // /* ///////           FOOTER     ///////// */
    <div
      style={{
        position: "relative",
        bottom: "0",
        width: "100%",
      }}
    >
      <div style={{ backgroundColor: "#f5f5f5" }}>
        <Container className="pt-2 pb-2" sx={{ backgroundColor: "#f5f5f5" }}>
          <Grid container>
            <Grid item xs={3}>
              <Typography sx={{ fontSize: { xs: "3vw", md: "1.8vw" } }}>
                <b>PRODUCTS</b>
              </Typography>
              {title.map(function (element, index) {
                return (
                  <Grid item xs={12} key={index}>
                    <Typography sx={{ fontSize: { xs: "2vw", md: "1.2vw" } }}>
                      {element}
                    </Typography>
                  </Grid>
                );
              })}
            </Grid>

            <Grid item xs={3}>
              <Typography sx={{ fontSize: { xs: "3vw", md: "1.8vw" } }}>
                <b>SERVICES</b>
              </Typography>
              {title.map(function (element, index) {
                return (
                  <Grid item xs={12} key={index}>
                    <Typography sx={{ fontSize: { xs: "2vw", md: "1.2vw" } }}>
                      {element}
                    </Typography>
                  </Grid>
                );
              })}
            </Grid>

            <Grid item xs={3}>
              <Typography sx={{ fontSize: { xs: "3vw", md: "1.8vw" } }}>
                <b>SUPPORT</b>
              </Typography>
              {title.map(function (element, index) {
                return (
                  <Grid item xs={12} key={index}>
                    <Typography sx={{ fontSize: { xs: "2vw", md: "1.2vw" } }}>
                      {element}
                    </Typography>
                  </Grid>
                );
              })}
            </Grid>

            <Grid item xs={3} align="center" className="pt-2">
              <Typography sx={{ fontSize: { xs: "3.4vw", md: "2.9vw" } }}>
                <b>GUITAR SHOP</b>
              </Typography>
              <Grid
                container
                className="d-flex flex-nowrap justify-content-center mt-2"
              >
                <Link
                  href="https://www.facebook.com/"
                  sx={{ mr: 2, color: "black" }}
                >
                  <FacebookIcon
                    sx={{ fontSize: { xs: "3.4vw", md: "2.4vw" } }}
                  />
                </Link>
                <Link
                  href="https://www.instagram.com/"
                  sx={{ mr: 2, color: "black" }}
                >
                  <InstagramIcon
                    sx={{ fontSize: { xs: "3.4vw", md: "2.4vw" } }}
                  />
                </Link>
                <Link
                  href="https://www.youtube.com/"
                  sx={{ mr: 2, color: "black" }}
                >
                  <YouTubeIcon
                    sx={{ fontSize: { xs: "3.4vw", md: "2.4vw" } }}
                  />
                </Link>
                <Link
                  sx={{ color: "black", width: { xs: "3.4vw", md: "2.4vw" } }}
                  href="https://twitter.com/"
                >
                  <TwitterIcon
                    sx={{ fontSize: { xs: "3.4vw", md: "2.4vw" } }}
                  />
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </div>
    </div>
  );
}

export default Footer;
