import Logo from "../../assets/images/logo.png";
import ModalLogIn from "../content/ModalLogIn";

import { useDispatch, useSelector } from "react-redux";

import {
  AppBar,
  Box,
  Toolbar,
  IconButton,
  Menu,
  MenuItem,
  Avatar,
  Tooltip,
  Container,
  Typography,
} from "@mui/material";

import MenuIcon from "@mui/icons-material/Menu";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import ListAltIcon from "@mui/icons-material/ListAlt";
import HomeIcon from "@mui/icons-material/Home";
import LoginIcon from "@mui/icons-material/Login";
import LogoutIcon from "@mui/icons-material/Logout";
import { Link } from "react-router-dom";

import { useEffect, useState } from "react";

import { auth, googleProvider } from "../../firebase";

const Header = () => {
  const [anchorElNav, setAnchorElNav] = useState(null);
  const [anchorElUser, setAnchorElUser] = useState(null);
  //MODAL LOGIN
  const [openModalLogIn, setOpenModalLogIn] = useState(false);
  // store
  const dispatch = useDispatch();
  const { userData } = useSelector((reduxData) => reduxData.taskReducer);

  const loginGoogle = () => {
    auth
      .signInWithPopup(googleProvider)
      .then((result) => {
        dispatch({
          type: "SET_VALUE_USER_DATA",
          payload: {
            userData: result.user,
          },
        });
        setOpenModalLogIn(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const logoutGoogle = () => {
    auth
      .signOut()
      .then(() => {
        dispatch({
          type: "SET_VALUE_USER_DATA",
          payload: {
            userData: null,
          },
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    auth.onAuthStateChanged((result) => {
      dispatch({
        type: "SET_VALUE_USER_DATA",
        payload: {
          userData: result,
        },
      });
    });
  }, [userData]);

  //MODAL
  const logIn = () => {
    setOpenModalLogIn(true);
  };
  const handleCloseModal = () => {
    setOpenModalLogIn(false);
  };
  //NAVBAR
  const handleOpenNavMenu = (event) => {
    setAnchorElNav(event.currentTarget);
  };
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };
  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };
  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  return (
    <div>
      <AppBar
        position="static"
        style={{
          background: "#333333",
        }}
      >
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Typography
              noWrap
              component="a"
              href="/"
              sx={{
                display: { xs: "none", md: "flex" },
                fontFamily: "monospace",
                fontWeight: 700,
                letterSpacing: ".3rem",
                color: "inherit",
                textDecoration: "none",
              }}
            >
              <img src={Logo} alt="Logo" width="100px" />
            </Typography>

            <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleOpenNavMenu}
                color="inherit"
              >
                <MenuIcon style={{ color: "#fff" }} />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorElNav}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "left",
                }}
                keepMounted
                transformOrigin={{
                  vertical: "top",
                  horizontal: "left",
                }}
                open={Boolean(anchorElNav)}
                onClose={handleCloseNavMenu}
                sx={{
                  display: { xs: "block", md: "none" },
                }}
              >
                <Link style={{ padding: "10px", color: "black" }} to={"/"}>
                  <HomeIcon />
                </Link>
                <Link
                  style={{ padding: "10px", color: "black" }}
                  to={"/products"}
                >
                  <ListAltIcon />
                </Link>{" "}
                <Link style={{ padding: "10px", color: "black" }} to={"/cart"}>
                  <AddShoppingCartIcon />
                </Link>
              </Menu>
            </Box>

            <Typography
              variant="h5"
              noWrap
              component="a"
              href="/"
              sx={{
                mr: 2,
                display: { xs: "flex", md: "none" },
                flexGrow: 1,
                fontFamily: "monospace",
                fontWeight: 700,
                letterSpacing: ".3rem",
                color: "inherit",
                textDecoration: "none",
              }}
            >
              <img src={Logo} alt="Logo" width="100" />
            </Typography>

            <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
              <Typography></Typography>
            </Box>

            <Box
              mr={2}
              justifyContent={"flex-end"}
              sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}
            >
              <Link style={{ padding: "10px", color: "#fff" }} to={"/"}>
                <HomeIcon />
              </Link>
              <Link style={{ padding: "10px", color: "#fff" }} to={"/products"}>
                <ListAltIcon />
              </Link>{" "}
              <Link style={{ padding: "10px", color: "#fff" }} to={"/cart"}>
                <AddShoppingCartIcon />
              </Link>
            </Box>

            {userData ? (
              <Box sx={{ flexGrow: 0 }}>
                <Tooltip title="Open settings">
                  <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                    <Avatar alt="Remy Sharp" src={userData.photoURL} />
                  </IconButton>
                </Tooltip>
                <Menu
                  sx={{ mt: "45px" }}
                  anchorEl={anchorElUser}
                  anchorOrigin={{ vertical: "top", horizontal: "right" }}
                  keepMounted
                  transformOrigin={{ vertical: "top", horizontal: "right" }}
                  open={Boolean(anchorElUser)}
                  onClose={handleCloseUserMenu}
                >
                  <MenuItem onClick={handleCloseUserMenu}>
                    <Typography variant="body1">
                      {userData.displayName}
                    </Typography>
                  </MenuItem>
                  <MenuItem onClick={handleCloseUserMenu}>
                    <Typography
                      variant="body1"
                      onClick={logoutGoogle}
                      color="primary"
                    >
                      <b>
                        <LogoutIcon /> Logout
                      </b>
                    </Typography>
                  </MenuItem>
                </Menu>
              </Box>
            ) : (
              <Box sx={{ flexGrow: 0 }}>
                <Typography
                  variant="body1"
                  onClick={logIn}
                  style={{ cursor: "pointer", color: "#43a047" }}
                >
                  <b className="text-login">
                    <LoginIcon /> Login
                  </b>
                </Typography>
              </Box>
            )}
          </Toolbar>
        </Container>
      </AppBar>

      {/* Modal LogIn */}
      <ModalLogIn
        openModalLogIn={openModalLogIn}
        handleCloseModal={handleCloseModal}
        loginGoogle={loginGoogle}
      />
    </div>
  );
};

export default Header;
