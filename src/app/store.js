import { createStore, combineReducers } from "redux";
import EventHandler from "../components/EventHandler";

const appReducer = combineReducers({
  taskReducer: EventHandler,
});

const store = createStore(appReducer, undefined, undefined);

export default store;
